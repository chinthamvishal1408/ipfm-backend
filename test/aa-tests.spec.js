const server = require('../app');
const chai = require('chai');
const http = require('chai-http');
const { default: mongoose } = require('mongoose');


//assertion style
const expect = chai.expect;

chai.use(http);

describe('aa-routes API tests',()=>{

    describe('POST /aa/consent/request',()=>{

        after(()=>{
            mongoose.connection.close();
        })

        before((done)=>{
            chai.request(server)
            .post('/aa/consent/request')
            .send({})
            .end((err,response)=>{
                done();
            })
        })

        it('should provide consentHandle when valid customerId is provided',(done)=>{
            const data = {customerId : '9999999999@onemoney'}
            chai.request(server)
            .post('/aa/consent/request')
            .send(data)
            .end((err,response)=>{
                expect(response.status,'status should be 200').to.equal(200)
                expect(response.body).to.be.a('object')
                .and.to.have.key('consentHandle')
                expect(response.body.consentHandle).to.match(/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}/)
                done();
            })
        }).timeout(0)

        it('should return an error if customerId not provided',(done)=>{
            const data = {}
            chai.request(server)
            .post('/aa/consent/request')
            .send(data)
            .end((err,response)=>{
                expect(response.status,'status should be 400').to.equal(400)
                expect(response.body).to.be.a('object')
                .and.to.have.key('error')
                expect(response.body.error).to.equal('customerId not provided')
                done();
            })
        })

        it('should return an error if provided customerId is invalid',(done)=>{
            const data = {customerId : 'absce@onemoney'}
            chai.request(server)
            .post('/aa/consent/request')
            .send(data)
            .end((err,response)=>{
                expect(response.status,'status should be 400').to.equal(400)
                expect(response.body).to.be.a('object')
                .and.to.have.key('error')
                expect(response.body.error).to.equal('invalid customerId is provided. customerId must be of the form: "customerMobileNumner@onemoney"')
                done();
            })

        })



    });


})


    // describe('GET /add/consent/handle/:handle',()=>{
    //     it('should return successful message if status of consent is ready',()=>{
    //         const handleId = 'validhandleId'
    //         chai.request(server)
    //         .get('/aa/consent/handle/'+handleId)
    //         .end((err,response)=>{
    //             // if(response.status==200){

    //             // }
    //         })
    //     })
    // })
