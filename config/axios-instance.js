const axios = require('axios');
require('dotenv').config();

const instance = axios.create({
    baseURL: (process.env.AA_PRODUCTION ? process.env.AA_PROD_URL : process.env.AA_SANDBOX_URL),
    timeout: 10000,
    headers: {'Client_API_Key': process.env.AA_CLIENT_API_KEY,   'Content-Type': 'application/json'}
});

module.exports = instance;
