const mongoose = require('mongoose');


const connectToDB = (databaseUrl)=>{
    return mongoose.connect(databaseUrl, {useNewUrlParser: true, useUnifiedTopology: true});
}

module.exports = connectToDB;