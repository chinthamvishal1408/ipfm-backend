// const httpStatusCodes = require('http-status-codes')


class InternalServerError extends Error{
  constructor(message){
    super(message)
      this.status = 500
      this.error = message;
  }
}
module.exports  = InternalServerError;