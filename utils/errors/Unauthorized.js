// const httpStatusCodes = require('http-status-codes')


class Unauthorized extends Error{
  constructor(message){
    super(message)
      this.status = 401
      this.error = message;
  }
}

module.exports  = Unauthorized;