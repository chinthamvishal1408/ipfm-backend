// const httpStatusCodes = require('http-status-codes')


class BadRequest extends Error{
  constructor(message){
      super(message)
      this.status = 400
      this.error = message;
  }
}

module.exports  = BadRequest;