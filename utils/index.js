const { v4: uuidv4 } = require('uuid');

exports.createUUID = () => uuidv4();