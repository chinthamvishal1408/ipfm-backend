const express = require("express");
const app = express();
const morgan = require("morgan"); // logger
const expressRateLimiter = require("express-rate-limit"); // request rate limiter
const helmet = require("helmet"); // extra security for express apps
require("dotenv").config();
const connectToDB = require('./config/db')
let databaseUrl = process.env.RDB_URL

// ROUTEs imports
const aaRoutes = require('./routes/aa-routes');
const aaNotificationRoute = require('./routes/aa-notification-routes');
if (process.env.PRODUCTION) {
    const rateLimiter = new expressRateLimiter({
        windowMs: 5 * 60 * 1000,
        max: 30,
        standardHeaders: true,
        legacyHeaders: false,
    });
    databaseUrl = process.env.RDBURL
    app.use(rateLimiter);
    
}

app.use(morgan("tiny"));
app.use(helmet());

// CORS handling
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header(
            "Access-Control-Allow-Methods",
            "PUT, POST, PATCH, DELETE, GET"
        );
        return res.status(200).json({});
    }
    next();
});

// body parsing
app.use(express.json())



//routes
app.use('/',aaNotificationRoute)
app.use('/aa',aaRoutes);
app.get("*",(req,res)=>{
    res.status(404).json()
})


const PORT =3000 || process.env.PORT;
const server = app.listen(PORT, async () => {
    console.log(`Server is listening at ${PORT}`);
    await connectToDB(databaseUrl)
    console.log('database is successfully connected!')
});

module.exports = server;
