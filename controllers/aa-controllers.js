const errors  = require('../utils/errors');
const axios = require('../config/axios-instance');
const utils = require('../utils');
require('dotenv').config();

//TODO: replace the catch block with some function call

exports.makeConsentRequest = async (req,res)=>{
    try{

        if(!('customerId' in req.body)) throw new errors.BadRequest('customerId not provided')
        if(req.body.customerId.match('^[0-9]{10}@onemoney$')==null) throw new errors.BadRequest('invalid customerId is provided. customerId must be of the form: "customerMobileNumner@onemoney"')

        const customerID = req.body.customerId;


        let date = new Date();
        let currentTimeStamp = new Date();
        // if(!process.env.AA_CONSENT_MONTHS) throw new errors.InternalServerError('AA_CONSENT_MONTHS env variable not set!')
        date.setMonth(date.getMonth() + Number(process.env.AA_CONSENT_MONTHS));
        let consentExpiryTimeStamp = date;

        const txnId = utils.createUUID();
        const aaVersion = process.env.AA_VERSION;
        const orgId = process.env.AA_ORG_ID;

        const fiStartDate = new Date(process.env.AA_FI_START_DATE);
        const fiEndDate = new Date(process.env.AA_FI_END_DATE);
        console.log(currentTimeStamp)
        const body =  {
            ver: aaVersion,
            timestamp: currentTimeStamp,
            txnid: txnId,
            ConsentDetail: {
                consentStart: currentTimeStamp,
                consentExpiry: consentExpiryTimeStamp,
                consentMode: "VIEW",
                fetchType: "ONETIME",
                consentTypes: [
                    "TRANSACTIONS",
                    "PROFILE",
                    "SUMMARY"
                ],
                fiTypes: [
                    "DEPOSIT"
                ],
                DataConsumer: {
                    "id": orgId
                },
                Customer: {
                    "id": customerID
                },
                Purpose: {
                    "code": "101",
                    "refUri": "https://api.rebit.org.in/aa/purpose/101.xml",
                    "text": "Wealth management service",
                    "Category": {
                        "type": "string"
                    }
                },
                FIDataRange: {
                    "from": fiStartDate,
                    "to": fiEndDate
                },
                DataLife: {
                    "unit": "MONTH",
                    "value": 0
                },
                Frequency: {
                    "unit": "MONTH",
                    "value": 1
                },
                DataFilter: [
                    {
                        "type": "TRANSACTIONAMOUNT",
                        "operator": ">=",
                        "value": "20000"
                    }
                ]
            }
          }

        
        const response = await axios.post('/aa/Consent',body);

        res.status(200).json({
            consentHandle : response.data.ConsentHandle
        })


    }catch(e){

        if(process.env.TESTING==undefined) console.log(e);
        res.status(e.status || 500).json({
            error: e.message || e.toString()
        })
    }
}


// exports.getConsentStatus = async(req,res)=>{
//     try{
//         const consentHandle = req.params.handle;
//         if(consentHandle==null) throw new errors.BadRequest('invalid consent handle');

//         const {data} = await axios.get('/aa/Consent/handle/'+consentHandle);
//             consentId = data.ConsentStatus.id;

//             const response = await axios.get('/aa/Consent/'+consentId);

//             const consentArtefact = response.data;

//             // SHOULDN'T SEND THE CONSENT DETAILS TO THE FRONTEND! DOING FOR TESTING PUPOSES
//             res.status(200).json({
//                 message: "Artefact retrieved and stored successfully",
//                 // consentArtefact //ConsentArtefact must not be public
//             })
//     }catch(e){
//         if(process.env.TESTING==undefined) console.log(e);
//         res.status(e.status || 500).json({
//             error: e.message || e.toString()
//         })
//     }
// }

